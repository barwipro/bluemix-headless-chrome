// you'll need to replace any calls to element.sendKeys(text) with
// driver.executeScript("arguments[0].setAttribute('value', text)", element);

ChromeOptions options = new ChromeOptions();
options.setBinary("/home/vcap/app/chrome/opt/google/chrome-beta/google-chrome-beta");
options.addArguments("headless");
options.addArguments("no-sandbox");
options.addArguments("disable-gpu");
DesiredCapabilities capabilities = DesiredCapabilities.chrome();
capabilities.setCapability(ChromeOptions.CAPABILITY, options);


RemoteWebDriver driver = new RemoteWebDriver(new URL("http://barte-test.eu-gb.mybluemix.net/"), capabilities);