#https://transfer.sh/klWJb/chrm.zip
import os
import time
from subprocess import call

chromePath = "/home/vcap/app/chrome/opt/google/chrome-beta/"
port = "--port=" + os.getenv('PORT', '5000')
os.environ['LD_LIBRARY_PATH'] = os.getenv('LD_LIBRARY_PATH', '') + ":" + chromePath

print("app port " + port)

call(["unzip", "-o", "chrm.zip"])
call([chromePath + "chromedriver", port, "--whitelisted-ips", "0.0.0.0"])